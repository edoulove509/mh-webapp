from shutil import move
from celery_progress.backend import ProgressRecorder

from nav_wordpress.models import NavItem
from nav_wordpress.navitem import img_processed
from .wpitem import WordpressItem
from webapp.celery import app as celery_app


@celery_app.task(bind=True, track_started=True)
def create_or_update_task(self, items):
    item = WordpressItem()
    progress_recorder = ProgressRecorder(self)

    for i in range(len(items)):
        if len(items) > 0:
            create_update = item.create_or_update_product(items[i])
            progress_recorder.set_progress(i + 1, len(items), items[i])
        else:
            print("There is no item to create")


@celery_app.task(bind=True, track_started=True)
def create_category_task(self):
    item = WordpressItem()
    item.create_all_category()


@celery_app.task(bind=True, track_started=True)
def img_processed_task(self):
    img_processed()


@celery_app.task(bind=True, track_started=True)
def change_status_task(self,list_item):
    item = WordpressItem()
    for product in list_item:
        item.change_product_status(product)

@celery_app.task(bind=True, track_started=True)
def fill_data_status_task(self):
    item = WordpressItem()
    item.getItemStatus()


@celery_app.task(bind=True, track_started=True)
def create_all_product_wp(self):
    products = NavItem.objects.all()
    item_wp = WordpressItem()
    for item in products:
        item_wp.create_or_update_product(item)

