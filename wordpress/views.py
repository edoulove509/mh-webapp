from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from nav_wordpress.local_db_method import create_or_update_images
from nav_wordpress.models import RetailProductGroups, Images
from webapp.config import FILEDATAPATH, WCAPI
from .tasks import create_or_update_task,  change_status_task
from celery.result import AsyncResult

from webapp.utils import getJpgFile, format_sku, namefromjpg
from wordpress.wpitem import WordpressItem


@login_required(login_url='login/')
def wp_product(request):
    item = WordpressItem()
    jpg = getJpgFile(FILEDATAPATH)
    it_sku = format_sku(jpg)
    print(it_sku)
    r = request.GET.get('select')

    item_wp = item.getItemOnWp(1)
    page = request.GET.get('page', 1)
    if request.GET.get('sku') is not None:
        _item = WCAPI.get('products', params={'sku': item}).json()
        return render(request, 'wp/wp_product.html', {'wpitem': _item, 'file': it_sku})
    paginator = Paginator(item_wp, 25)
    try:
        _item = paginator.page(page)
    except PageNotAnInteger:
        _item = paginator.page(1)
    except EmptyPage:
        _item = paginator.page(paginator.num_pages)
    print(r)

    if request.method == 'POST':
        list_sku = request.POST.getlist('checkbox')
        wp_create_task = create_or_update_task.delay(list_sku)

        result = AsyncResult(wp_create_task.task_id)
        print(result.state)
        request.session['task_id'] = wp_create_task.task_id
        request.session['len_list_sku'] = len(list_sku)

        response_data = {
            'state': result.state,
            'details': result.info,
            'status': result.status
        }
        return redirect('success-create-wp-item')
    return render(request, 'wp/wp_product.html', {'wpitem': _item, 'file': it_sku})


@login_required(login_url='login/')
def success_wp_item(request):

    if request.session.has_key('task_id'):
        task_id = request.session['task_id']
        result = AsyncResult(task_id)
        # namefile= request.session['namefile']
        len_list_sku = request.session['len_list_sku']
        list_sku = request.session['list_sku']

        print(list_sku)
        if request.method == "POST":
            change_stat_task = change_status_task.delay(format_sku(list_sku))
            t_id = change_stat_task.task_id
            request.session['t_id'] = t_id
            request.session['list_sku_success'] = list_sku
            return redirect("wp-product-manage")
        print(result.state)
        return render(request, 'wp/success_wp.html', {'task_id': task_id, 'len_list_sku': len_list_sku, 'list_sku':list_sku})

    # r = request.POST.get('task_id')
    # print(r)



    return render(request, 'wp/success_wp.html')


@login_required(login_url='login/')
def create_wp_item(request):
    if request.method == 'POST':
        list_img = []
        list_sku = []
        images = request.FILES.getlist('image')
        print(images)
        for img in images:
            print(img.name)
            filename = img.name
            f_name = namefromjpg(filename)
            list_img.append(filename)
            create_or_update_images(name=f_name, image=img)
            list_sku.append(filename)

        wp_create_task = create_or_update_task.delay(format_sku(list_img))

        result = AsyncResult(wp_create_task.task_id)
        print(result.state)
        request.session['task_id'] = wp_create_task.task_id
        request.session['len_list_sku'] = len(list_img)
        request.session['list_sku'] = list_sku

        # response_data = {
        #     'state': result.state,
        #     'details': result.info,
        #     'status': result.status
        # }
        return redirect('success-create-wp-item')
        # return HttpResponse(json.dumps({'message': "message"}))
    return render(request, 'wp/create_wp.html')


@login_required(login_url='login/')
def update_wp_item(request):
    return render(request, 'wp/update_wp.html')


@login_required(login_url='login/')
def detail_wp_item(request):
    if request.GET.get('sku') is not None:
        sku = request.GET.get('sku')
        _item = WCAPI.get('products', params={'sku': sku}).json()
        print(_item)
        return render(request, 'wp/item_detail.html', {'wpitem': _item})

    return render(request, "wp/item_detail.html")


@login_required(login_url='login/')
def editItem(request):
    if request.GET.get('sku') is not None:
        sku = request.GET.get('sku')
        _item = WCAPI.get('products', params={'sku': sku}).json()
        print(_item)
        return render(request, 'wp/edit_item_wp.html', {'wpitem': _item})

    return render(request, "wp/edit_item_wp.html")


@login_required(login_url='login/')
def create_category(request):
    if request.method == 'POST':
        dict_cat = {}
        all_retail_group = RetailProductGroups.objects.all()
        print(all_retail_group)
        for pg in all_retail_group:
            print(pg.categoryName)
            dict_cat[pg.categoryName] = pg.description

        # create_cat_task = create_category_task.delay(dict_cat)
        return redirect('create-cat-success')

    return render(request, "wp/wp_create_cat.html")


@login_required(login_url='login/')
def create_cat_success(request):
    return render(request, "wp/create_cat_success.html")


@login_required(login_url='login/')
def wp_manage_products(request):
    print(request.session.has_key('list_sku_success'))
    if request.session.has_key('t_id'):
        list_sku = request.session['list_sku_success']
        t_id = request.session['t_id']
        print("LIST", list_sku)
        item = WordpressItem()
        # list_item = item.getItemOnWp(format_sku(list_sku))
        # print(list_item)
        return render(request, "wp/manage_products.html", {"t_id": t_id})

    # if request.method == "POST":
    #     list_sku = request.POST.getlist('status')
    #     print(list_sku)
    #     change_stat_item_task = change_status_task.delay(list_sku)
    #     return redirect('wp-product-change-status-success')

    return render(request, "wp/manage_products.html")


@login_required(login_url='login/')
def wp_change_status_success(request):
    return render(request, "wp/change_status_success.html")
