from django.urls import path,re_path,include
from . import views

urlpatterns = [
    path('wp-product/', views.wp_product, name = 'wp-product'),
    path('wp-product/create-wp-item', views.create_wp_item, name = 'create-wp-item'),
    path('wp-product/create-wp-item/success-created', views.success_wp_item, name = 'success-create-wp-item'),
    path('wp-product/update-wp-item', views.update_wp_item, name = 'update-wp-item'),
    path('wp-product/detail-wp-item', views.detail_wp_item, name = 'detail-wp-item'),
    path('wp-product/edit-wp-item', views.editItem, name = 'edit-wp-item'),
    path('wp-product/create-wp-category', views.create_category, name = 'create-wp-category'),
    path('wp-product/create-wp-category/success', views.create_cat_success, name='create-cat-success'),
    path('wp-product/manage', views.wp_manage_products, name='wp-product-manage'),
    path('wp-product/change-status/success', views.wp_change_status_success, name='wp-product-change-status-success'),

]