import base64
import requests
from nav.NavItemHierarchy import NavItemHierarchy
from nav_wordpress.models import RetailProductGroups, NavItem
from nav_wordpress.navitem import create_or_update_local_item
import html
from os import path
from webapp.config import WCAPI, USER_WORDPRESS, PASSWORD_AUTH, CONSUMER_KEY, CONSUMER_SECRET, TOP_URL_IMG, \
    TOP_URL_PRODUCT, IMAGEPRODPATH
from webapp.utils import getJpgFile, format_sku, itemExistOnWp, check_image_set, check_category_set


class WordpressItem(object):

    def __init__(self):
        self.path = "C:\\Users\\Mhandal\\Documents\\MHDATA\\2-3-2021"
        # initialization of woocommerce api
        self.wcapi = WCAPI
        self.item_hierarchy = NavItemHierarchy()

        # --- GET ITEMS ON WOOCOMMERCE METHOD ---

    def getItemOnWp(self, *args):
        print("_____________GET ITEM ON WP _______")
        process_done = False
        if (args[0] == 1):
            products = self.wcapi.get('products', params={'per_page': 100, 'page': args[0]}).json()
            return products
        else:
            print(len(args[0]))
            str_sku = ""
            for it in args[0]:
                str_sku = str_sku + it + ","
            print(str_sku)

            products = self.wcapi.get('products', params={'sku': str_sku, 'per_page': 100, 'page': 1}).json()
            if len(products) > 0:
                return products

    # --- GET ITEMS ON A LOCAL FILE  ---
    def getItemOnFile(self):
        tot_page_req = self.wcapi.get("products").headers['X-WP-Total']
        num_lines = sum(1 for line in open('wordpress\\data\\data.txt'))
        file = open("wordpress\\data\\data.txt", "r+")
        file.truncate(0)
        page = 1
        while True:
            products = self.wcapi.get('products', params={'per_page': 100, 'page': page}).json()
            # print(products)
            if len(products) == 0:
                break
            else:
                for prod in products:
                    file.write(prod['sku'] + '\n')
                page = page + 1
        file.close()

        # --- GET ITEMS WITH STATUS  ---

    def getItemStatus(self):
        list_item_private = []
        file = open("wordpress\\data\\data_status.txt", "r+")
        file.truncate(0)
        page = 1
        while True:
            products = self.wcapi.get('products', params={'per_page': 100, 'page': page}).json()
            if len(products) == 0:
                break
            else:
                for prod in products:
                    if prod['status'] == "private":
                        print("Begin")
                        file.write(prod['sku'] + '\n')
                        # list_item_private.append(prod['sku'])
                page = page + 1
        file.close()
        # return list_item_private

    # --- UPLOAD IMAGES ON WOOCOMMERCE METHOD ---
    def uploadImages(self, name, imgPath):
        print("_____________UPLOADING IMAGES...._______")

        user = USER_WORDPRESS  # the user in which the auth. token is given
        pythonapp = PASSWORD_AUTH  # auth password
        url = TOP_URL_IMG  # the url of the wp access location
        token = base64.standard_b64encode((user + ':' + pythonapp).encode('utf-8'))  # we have to encode the usr and pw
        headers = {'Authorization': 'Basic ' + token.decode('utf-8')}

        media = {'file': open(imgPath, 'rb')}  # 'picture.jpg' path to the image
        print(("BEGIN"))
        try:
            image_get = requests.get(url + "media/?slug=" + name)
            print(image_get)
            print(image_get.json())
            if len(image_get.json()) > 0:

                if str(image_get.json()[0]['slug']) != name:
                    try:
                        image = requests.post(url + 'media', headers=headers, files=media)
                        if image.status_code == 201:
                            response = image.json()
                            id_image = response['id']
                            url_image = response['guid']['raw']
                            return [id_image, url_image, "image created"]
                    except Exception as e:
                        raise e
                else:
                    id_image = image_get.json()[0]['id']
                    url_image = image_get.json()[0]['guid']['rendered']
                    return [id_image, url_image, "image already exists"]
            else:
                try:
                    image = requests.post(url + 'media', headers=headers, files=media)
                    if image.status_code == 201:
                        response = image.json()
                        id_image = response['id']
                        url_image = response['guid']['raw']
                        return [id_image, url_image, "image created"]
                        print("Image Created")
                except Exception as e:
                    raise e

        except Exception as e:
            raise e

    # --- CHECK  ATTRIBUTES EXIST ---
    def attrib_exist(self, name):
        print("_____________CHECKING ATTRIBUTES EXIST______")

        data = {
            "name": name,
        }

        # page = 1
        result = 0
        att_request = self.wcapi.get('products/attributes/').json()

        # print(page)
        if len(att_request) == 0:
            print("REsult NOt Ok")
        # print(cat_request)
        if len(att_request) > 0:
            print(len(att_request))
            for att in att_request:
                if html.unescape(att['name']) == name:
                    print(att['name'])
                    result = [True, att['id'], att['name']]
                    return result
                    break

                else:
                    result = [False]

        return result

    # --- GET OR CREATE ATTRIBUTES FOR ITEMS  METHOD ---
    def get_or_create_Attribute(self, name):
        print("_____________GET OR CREATE  ATTRIBUTES______")

        data = {
            "name": name,
        }

        attrib = self.attrib_exist(name)
        if not attrib[0]:
            try:
                req_create_attrib = self.wcapi.post("products/attributes", data)
                if req_create_attrib.status_code == 201:
                    req = req_create_attrib.json()
                    id_attrib = req['id']
                    return id_attrib
            except Exception as e:
                raise e
        else:
            id_attrib = attrib[1]
            return id_attrib

    # --- CHECK  CATEGORY EXIST ---
    def category_exist(self, name):
        print("_____________CHECKING CATEGORY EXIST______")

        data = {
            "name": name,
        }

        page = 1
        result = 0
        while True:
            cat_request = self.wcapi.get('products/categories/', params={'per_page': 10, 'page': page}).json()
            if len(cat_request) == 0:
                break
            if len(cat_request) > 0:
                for cat in cat_request:
                    if html.unescape(cat['name']) == name:
                        print(cat['name'])
                        result = [True, cat['id']]
                        return result
                        break
                    else:
                        result = [False]

            page = page + 1
        return result

    # --- GET CATEGORY ITEMS METHOD ---
    def get_or_create_category(self, name):
        cat_exist = self.category_exist(name)
        print(cat_exist)
        print(f"____ GET OR CREATE CATEGORY _____{name}")
        if not name == "GENERAL":
            if not cat_exist[0]:
                try:
                    req_create_cat = self.wcapi.post("products/categories", data={"name": name})
                    if req_create_cat.status_code == 201:
                        req = req_create_cat.json()
                        id_cat = req['id']
                        return id_cat
                except Exception as e:
                    print("Can't create category ")
                    print(req_create_cat.json())
            else:
                id_cat = cat_exist[1]
                return id_cat

    #  --- CREATE ITEM WOOCOMMERCE METHOD ---
    def createItem(self, **kwargs):
        print("_____________CREATING PRODUCT WP______")

        sku = kwargs['sku']
        name = kwargs['name']
        description = kwargs['description']

        regular_price = kwargs['regular_price']
        category_list = kwargs['category_list']
        attributes = kwargs['attributes']
        inventory = kwargs['inventory']
        # get the attributes
        attrib_list = []
        cat_list = []

        if len(attributes) == 0:
            attrib_list = []
        elif len(attributes) > 0:
            for i in range(len(attributes)):
                attrib_list.append({
                    "id": attributes[i][0],
                    # "name":attributes[i][0]['name'],
                    "position": i,
                    "options": [attributes[i][1]],
                    "visible": True,

                })
        if len(category_list) > 0:
            for cat_id in category_list:
                cat_list.append(
                    {
                        "id": cat_id
                    }
                )

        # Data to post on woocommerce
        data = {
            "sku": sku,
            "name": name,
            "description": description,
            "regular_price": regular_price,
            "stock_quantity": inventory,
            "manage_stock": True,
            "status": "private",
            "categories": cat_list,
            "attributes": attrib_list
        }
        # post data to the woocommerce API
        try:
            created_product = self.wcapi.post("products", data).json()
        except Exception as e:
            raise e
        return created_product

    # --- UPDATE ITEM WOOCOMMERCE METHOD
    def updateItem(self, **kwargs):
        print("_____________UPDATING PRODUCT WP______")

        sku = kwargs['sku']
        name = kwargs['name']
        description = kwargs['description']

        regular_price = kwargs['regular_price']
        category_list = kwargs['category_list']
        attributes = kwargs['attributes']
        inventory = kwargs['inventory']
        # get the attributes
        attrib_list = []
        cat_list = []
        if len(attributes) == 0:
            attrib_list = []
        elif len(attributes) > 0:
            for i in range(len(attributes)):
                attrib_list.append({
                    "id": attributes[i][0],
                    # "name":attributes[i][0]['name'],
                    "position": i,
                    "options": [attributes[i][1]],
                    "visible": True,

                })
        if len(category_list) > 0:
            for cat_id in category_list:
                cat_list.append(
                    {
                        "id": cat_id
                    }
                )

        # Data to post on woocommerce
        data = {

            "name": name,
            "description": description,
            "regular_price": regular_price,
            "stock_quantity": inventory,
            "manage_stock": True,
            "categories": cat_list,
            "attributes": attrib_list
        }
        # get the id of the iem by sku before updating products
        req = self.wcapi.get("products", params={'sku': sku}).json()
        id = req[0]['id']
        try:
            update = self.wcapi.put("products/" + str(id), data).json()
        # print(update)
        except Exception as e:
            raise e
        return update

    # --- MAIN METHOD TO CREATE OR UPDATE A FULL WOOCOMMERCE PRODUCT ---
    def create_or_update_product(self, item):
        print(item)
        try:
            nav_item = NavItem.objects.get(no=item)
            if nav_item is not None:
                # create or update Attributes and category
                list_attribute = []
                list_category = []
                if nav_item.attributeValue is not None:
                    if len(nav_item.attributeValue.all()) > 0:
                        for att in nav_item.attributeValue.all():
                            # if not att['Attribute_Code']=='WEB':
                            id_attrib = self.get_or_create_Attribute(att.attributeCode.code)
                            # here we append info we ll need into a list like id, name of attributes nd attributes terms
                            list_attribute.append([id_attrib, att.attributeValue.option_value])

                else:
                    list_attribute = []
                # category_name =
                # category_description =
                if nav_item.retailProductGroup is not None:
                    if nav_item.retailProductGroup.categoryName and nav_item.retailProductGroup.description is not None:
                        id_cat_1 = self.get_or_create_category(nav_item.retailProductGroup.categoryName)
                        id_cat_2 = self.get_or_create_category(nav_item.retailProductGroup.description)
                        list_category.append(id_cat_1)
                        list_category.append(id_cat_2)

                sku = nav_item.no
                name = nav_item.description
                description = nav_item.description  # To discuss cause --Attributes--
                inventory = nav_item.inventory
                unitPrice = nav_item.unitPrice
                itemPrice = nav_item.itemPrice
                price = 0
                if itemPrice.count() > 0:
                    price = nav_item.itemPrice.all()[0]
                else:
                    price = nav_item.unitPrice
                if not itemExistOnWp(item=item):
                    # path_img = IMAGEPRODPATH  + item + ".jpg"
                    # up_img = self.up_local_img_2_wp(item,path_img)
                    # print(up_img)
                    create_it = self.createItem(sku=sku, name=name, description=description, inventory=str(inventory),
                                                regular_price=str(price), category_list=list_category,
                                                attributes=list_attribute)
                    return "Successfully created"
                else:

                    update_it = self.updateItem(sku=sku, name=name, description=description, inventory=str(inventory),
                                                regular_price=str(price), category_list=list_category,
                                                attributes=list_attribute)

                    return "Successfully updated"
        except Exception as e:
            print(f"{item} isn't a Local Item")

    def set_cat_parent(self, id_parent, id_son):
        data = {
            "parent": id_parent
        }
        req = self.wcapi.put(f"products/categories/{id_son}", data).json()

    # METHOD  TO CREATE ALL CATEGORY FROM LOCAL DB TO WORDPRESS
    def create_all_category(self):
        prod_group = RetailProductGroups.objects.all()
        for pg in prod_group:
            id_cat_parent = 0
            id_cat_son = 0
            if pg.categoryName is not None:
                id_cat_parent = self.get_or_create_category(pg.categoryName)
            if pg.description is not None:
                print(pg.description)
                id_cat_son = self.get_or_create_category(pg.description)
            self.set_cat_parent(id_cat_parent, id_cat_son)
            print([id_cat_parent, id_cat_son, "OK!"])

    CONSUMER_KEY

    # CHECK AND CHANGE STATUS OF PRODUCT
    def change_product_status(self, item):
        data = {
            "status": "publish",
        }
        img_set = check_image_set(item)
        cat_set = check_category_set(item)[0]
        id_item = check_category_set(item)[1]
        print(cat_set, img_set)
        if img_set and cat_set:
            try:
                update = self.wcapi.put("products/" + str(id_item), data).json()
                print(update)
                print(f"{item} set to publish status")
            except Exception as e:
                print(e)

    def up_local_img_2_wp(self, name, path_img):
        if path.exists(path_img):
            id_url = self.uploadImages(name, path_img)
            return id_url
        else:
            return None
