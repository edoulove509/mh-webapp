from django.shortcuts import render, redirect

from webapp.utils import format_sku
from .models import Images


def local_product(request):
    return render(request, "nav_wordpress/local_product.html")


def upload_image(request):
    if request.method == 'POST':
        images = request.FILES.getlist('image')
        print(images)
        for img in images:
            # print(img)
            filename = img.name
            name = format_sku(filename)
            post_img = Images.objects.create(name=filename, image=img)

        return redirect('upload-success')

    return render(request, "nav_wordpress/upload_image.html")


def upload_success(request):
    return render(request, 'nav_wordpress/upload_success.html')


def task(request):


    return render(request, 'nav_wordpress/task.html')

def galery(request):
    images = Images.objects.all()

    return render(request, 'nav_wordpress/galery.html',{"images":images})