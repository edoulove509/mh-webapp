from django.contrib import admin
from .models import (NavItem, ItemPrices, Attributes, AttributeValues, ItemTranslations,
                     RetailProductGroups,
                     AttributeOptionValues, Images)

# Register your models here.

admin.site.register(NavItem)
admin.site.register(ItemPrices)
admin.site.register(AttributeValues)
admin.site.register(ItemTranslations)
admin.site.register(AttributeOptionValues)
admin.site.register(RetailProductGroups)
admin.site.register(Attributes)
admin.site.register(Images)
