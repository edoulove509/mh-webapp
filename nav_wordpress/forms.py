# forms.py
from django import forms
from webapp.config import MAX_SIZE_FILE
from webapp.utils import c_bytes


class ImagesForm(forms.Form):
    image = forms.ImageField(widget=forms.ClearableFileInput(attrs={'multiple': True, 'name': 'image'}))

    def clean(self):
        cleanned_data = super(ImagesForm, self).clean()
        image = cleanned_data.get("image")
        if image:
            if image.size > MAX_SIZE_FILE:
                self.add_error('file', 'File too big, max size is %(MAX_SIZE_FILE)s' % {
                    "MAX_SIZE_FILE": c_bytes(MAX_SIZE_FILE)})
        else:
            self.add_error("image", "required field")
