from django.db import models
from datetime import datetime


# Create your models here.

class Images(models.Model):
    name = models.CharField(max_length=10,unique=True)
    image = models.ImageField(upload_to='products/', blank=True)
    date_created = models.DateTimeField(default=datetime.now, blank=True)
    lastSyncDate = models.DateTimeField(null=True)
    def __str__(self):
        return self.name

class RetailProductGroups(models.Model):
    code = models.CharField(max_length=10, unique=True)
    description = models.CharField(max_length=50, blank=True)
    itemCategoryCode = models.CharField(max_length=50)
    categoryName = models.CharField(max_length=50, blank=True)
    date_created = models.DateTimeField(default=datetime.now, blank=True)
    lastSyncDate = models.DateTimeField(null=True)

    def __str__(self):
        return self.code


class Attributes(models.Model):
    code = models.CharField(max_length=10, unique=True)
    description = models.CharField(max_length=50, blank=True)
    valueType = models.CharField(max_length=15, blank=True)
    date_created = models.DateTimeField(default=datetime.now, blank=True)
    lastSyncDate = models.DateTimeField(null=True)

    def __str__(self):
        return self.code


class AttributeOptionValues(models.Model):
    option_value = models.CharField(max_length=250, unique=True)
    date_created = models.DateTimeField(default=datetime.now, blank=True)
    lastSyncDate = models.DateTimeField(null=True)

    def __str__(self):
        return self.option_value


class ItemPrices(models.Model):
    salesCode = models.CharField(max_length=20)
    unitPrice2 = models.DecimalField(max_digits=10, decimal_places=2, blank=True)
    unitPriceIncludingVAT = models.DecimalField(max_digits=10, decimal_places=2, blank=True)
    minimumQuantity = models.DecimalField(max_digits=10, decimal_places=2, blank=True)
    startingDate = models.CharField(max_length=10, blank=True, null=True)
    endingDate = models.CharField(max_length=10, blank=True, null=True)
    date_created = models.DateTimeField(default=datetime.now, blank=True)
    lastSyncDate = models.DateTimeField(null=True)

    def __str__(self):
        return self.salesCode


class AttributeValues(models.Model):
    attributeCode = models.ForeignKey(Attributes, on_delete=models.CASCADE, related_name='attribute_attribute_values')
    attributeValue = models.ForeignKey(AttributeOptionValues, on_delete=models.CASCADE,
                                       related_name='attributes_options_values_attribute_values')
    numericValue = models.DecimalField(max_digits=10, decimal_places=2)
    VALUE_CALCULATED_CHOICES = [('Yes', 'No')]
    valueCalculated = models.CharField(max_length=5, choices=VALUE_CALCULATED_CHOICES)
    hardAttribute = models.BooleanField()
    date_created = models.DateTimeField(default=datetime.now, blank=True)
    lastSyncDate = models.DateTimeField(null=True)

    def __str__(self):
        return str(self.attributeCode) + " " + str(self.attributeValue)


class ItemTranslations(models.Model):
    variantCode = models.CharField(max_length=10, blank=True, null=True)
    languageCode = models.CharField(max_length=10)
    description = models.CharField(max_length=50, blank=True, null=True)
    description2 = models.CharField(max_length=50, blank=True, null=True)
    date_created = models.DateTimeField(default=datetime.now, blank=True)
    lastSyncDate = models.DateTimeField(null=True)

    def __str__(self):
        return self.variantCode


class NavItem(models.Model):
    no = models.CharField(max_length=20, unique=True)
    photo = models.ManyToManyField(Images,  related_name='images_nav_item', blank=True,null=True)
    description = models.CharField(max_length=50)
    retailProductGroup = models.ForeignKey(RetailProductGroups, on_delete=models.CASCADE,
                                           related_name='retail_product_group_nav_item', blank=True,null=True)
    baseUnitOfMeasure = models.CharField(max_length=10, blank=True, null=True)
    salesUnitOfMeasure = models.CharField(max_length=10, blank=True, null=True)
    itemCategoryCode = models.CharField(max_length=100, blank=True, null=True)
    unitPrice = models.DecimalField(max_digits=10, decimal_places=2)
    itemPrice = models.ManyToManyField(ItemPrices, related_name='item_prices_nav_item', blank=True)
    grossWeight = models.DecimalField(max_digits=6, decimal_places=2)
    netWeight = models.DecimalField(max_digits=6, decimal_places=2)
    blocked = models.BooleanField(default=False)
    inventory = models.DecimalField(max_digits=10, decimal_places=2)
    width = models.DecimalField(max_digits=6, decimal_places=2)
    height = models.DecimalField(max_digits=6, decimal_places=2)
    depth = models.DecimalField(max_digits=6, decimal_places=2)
    attributeValue = models.ManyToManyField(AttributeValues, related_name='attributes_values_nav_item', blank=True)
    itemtranslation = models.ManyToManyField(ItemTranslations, related_name='item_translations_nav_item', blank=True)
    date_created = models.DateTimeField(default=datetime.now, blank=True)
    lastSyncDate = models.DateTimeField(null=True)

    def __str__(self):
        return self.no
