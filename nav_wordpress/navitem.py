from shutil import move
from os import path
from django.conf import settings

from nav.NavItemHierarchy import NavItemHierarchy
from datetime import datetime

from nav_wordpress.local_db_method import (create_or_update_ret_prod_group, create_or_update_attributes,
                                           create_or_update_attrib_opt_values, create_or_update_attrib_values,
                                           create_or_update_sales_price,
                                           create_or_update_nav_item, create_or_update_images)

from webapp.utils import getJpgFile, format_sku, check_image_set

item_hierarchy = NavItemHierarchy()


def create_or_update_local_item(item):
    lastSyncDate = datetime.now()
    nav_item = item_hierarchy.get_items(no=item)
    if nav_item is not None:

        print("_____________NAVITEM__CREATION_____", nav_item)
        list_attribut = []
        list_sales_price = []
        product_group = None

        # create or update Attributes
        dict_attrib_value = {}
        attrib_nav = []
        if nav_item['AttributeValues'] is not None:
            attrib_nav = nav_item['AttributeValues']['Attribute_Values']
        else:
            attrib_nav = "Attributes"
        sku = nav_item['No']
        description_nav_item = nav_item['Description']  # To discuss cause --Attributes--
        product_group_code = nav_item['Product_Group_Code']
        baseUnitOfMeasure = nav_item['_x003C_Base_Unit_of_Measure_x003E_']
        salesUnitOfMeasure = nav_item['Sales_Unit_of_Measure']
        itemCategoryCode = nav_item['Item_Category_Code']
        unitPrice = nav_item['Unit_Price']
        # itemPrice = nav_item['Description']
        grossWeight = nav_item['Gross_Weight']
        netWeight = nav_item['Net_Weight']
        inventory = nav_item['Inventory']

        print(product_group_code)

        if product_group_code is not None:
            product_groups_response = item_hierarchy.get_product_groups(code="Code" ,product_group_code=product_group_code)
            for pg in product_groups_response:
                id_product_group = create_or_update_ret_prod_group(
                    code=pg.Code,
                    description=pg.Description,
                    itemCategoryCode=pg.Item_Category_Code,
                    categoryName=pg.Category_Name,
                    date_created=datetime.now,
                    lastSyncDate=lastSyncDate,
                )
                product_group = id_product_group
            print(product_group)

        if nav_item['AttributeValues'] is not None:
            for att in nav_item['AttributeValues']['Attribute_Values']:
                if not att['Attribute_Code'] == 'WEB':
                    Numeric_Value = att['Numeric_Value']
                    Value_Calculated = att['Value_Calculated']
                    Hard_Attribute = att['Hard_Attribute']
                    print(att['Attribute_Code'])
                    id_attrib = create_or_update_attributes(
                        code=att['Attribute_Code'],
                        description=att['Attribute_Code'],
                        valueType=att['Attribute_Code'],
                        date_created=datetime.now,
                        lastSyncDate=lastSyncDate,
                    )
                    print(id_attrib)
                    id_attrib_opt_value = create_or_update_attrib_opt_values(
                        option_value=att['Attribute_Value'],
                        date_created=datetime.now,
                        lastSyncDate=lastSyncDate,
                    )
                    print(id_attrib_opt_value)
                    dict_attrib_value[id_attrib] = [id_attrib_opt_value, Numeric_Value, Value_Calculated,
                                                    Hard_Attribute]

        else:
            dict_attrib_value = {}

        if len(dict_attrib_value) > 0:
            for key, value in dict_attrib_value.items():
                print(key, value[0])
                id_attrib_value_with_option = create_or_update_attrib_values(
                    attributeCode=key,
                    attributeValue=value[0],
                    numericValue=value[1],
                    valueCalculated=value[2],
                    hardAttribute=value[3],
                    date_created=datetime.now,
                    lastSyncDate=lastSyncDate,
                )
                list_attribut.append(id_attrib_value_with_option)

        if len(nav_item['SalesPrice']['Item_Prices_NF']) > 0:
            for price in nav_item['SalesPrice']['Item_Prices_NF']:
                id_salesprice = create_or_update_sales_price(
                    salesCode=price['Sales_Code'],
                    unitPrice2=price['UnitPrice2'],
                    unitPriceIncludingVAT=price['Unit_Price_Including_VAT'],
                    minimumQuantity=price['Minimum_Quantity'],
                    startingDate=price['Starting_Date'],
                    endingDate=price['Starting_Date'],
                    date_created=datetime.now,
                    lastSyncDate=lastSyncDate
                )
                list_sales_price.append(id_salesprice)
        price = 0
        if len(nav_item['SalesPrice']) > 0:
            price = nav_item['SalesPrice']['Item_Prices_NF'][0]['UnitPrice2']
        else:
            price = nav_item['Unit_Price']
        if nav_item['Attrib_1_Code']=="Yes":

            create = create_or_update_nav_item(
                no=sku,
                description=description_nav_item,
                retailProductGroup=product_group,
                baseUnitOfMeasure=baseUnitOfMeasure,
                salesUnitOfMeasure=salesUnitOfMeasure,
                itemCategoryCode=itemCategoryCode,
                unitPrice=unitPrice,
                itemPrice=list_sales_price,
                grossWeight=grossWeight,
                netWeight=netWeight,
                blocked=nav_item['Blocked'],
                inventory=inventory,
                width=nav_item['Width'],
                height=nav_item['Height'],
                depth=nav_item['Depth'],
                attributeValue=list_attribut,
                date_created=datetime.now(),
                lastSyncDate=datetime.now(),

            )
            return create

def create_or_update_product_group():
    lastSyncDate = datetime.now()
    list_post_pd = []
    description = ""
    itemCategoryCode = ""
    categoryName = ""
    product_group = item_hierarchy.get_product_groups(code="", product_group_code="")
    for pd in product_group:
        code = pd.Code
        if pd.Description is not None:
            description = pd.Description
        if pd.Item_Category_Code is not None:
            itemCategoryCode = pd.Item_Category_Code
        if pd.Category_Name is not None:
            categoryName = pd.Category_Name
            pd_create = create_or_update_ret_prod_group(code=code, description=description, categoryName=categoryName,
                                                        itemCategoryCode=itemCategoryCode,
                                                        lastSyncDate=lastSyncDate)
            list_post_pd.append({pd_create: code})
    return list_post_pd


def img_processed():
    path_img = settings.MEDIA_ROOT+"/products/"
    jpg = getJpgFile(path_img)
    list_name = format_sku(jpg)
    print(list_name)
    for name in list_name:
        # get_img = Images.objects.get(name=name)
        # img_path = get_img.image
        # print(img_path)

        img_check = check_image_set(name)
        print(img_check)
        if img_check:
            path1 = path_img + name + ".jpg"
            if  path.exists(path1):
                print(path1)
                path2 = path_img + "/processed/" + name + ".jpg"
                print(path2)
                move(path1, path2)
                create_or_update_images(name=name,image=path2)

def createUpdateAllProduct():
    list_item = item_hierarchy.get_items(field="Attrib_1_Code",criteria="Yes", size=0)
    for item in list_item:
        print(item)
        create_or_update_local_item(item['No'])




