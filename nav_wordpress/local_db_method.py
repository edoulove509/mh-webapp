
from datetime import datetime

from nav_wordpress.models import (Images, RetailProductGroups, Attributes, AttributeOptionValues, ItemPrices,
	AttributeValues, ItemTranslations, NavItem)


def create_or_update_images(**kwargs):
	name = kwargs['name']
	image = kwargs['image']
	get_image = Images.objects.filter(name=name)
	if not get_image:
		create_img = Images.objects.create(
				name=name,
				image=image,
				lastSyncDate = datetime.now()

			)
		ID = Images.objects.get(name=name)
		return ID.id
	else:
		update_image = Images.objects.get(name=name)
		update_image.image = image
		update_image.lastSyncDate = datetime.now()
		return update_image.id

def create_or_update_ret_prod_group(**kwargs):
	code = kwargs['code']
	description = kwargs['description']
	itemCategoryCode = kwargs['itemCategoryCode']
	categoryName = kwargs['categoryName']
	lastSyncDate = kwargs['lastSyncDate']

	get_product_group = RetailProductGroups.objects.filter(code=code)
	if not get_product_group:
		create_pd = RetailProductGroups.objects.create(
				code=code,
				description=description,
				itemCategoryCode=itemCategoryCode,
				categoryName=categoryName,
				# date_created=date_created,
				lastSyncDate = lastSyncDate
			)
		ID =  RetailProductGroups.objects.get(code=code)
		return ID.id

	else:
		update_pd = RetailProductGroups.objects.filter(code=code).update(
				code=code,
				description=description,
				itemCategoryCode=itemCategoryCode,
				categoryName=categoryName,
				lastSyncDate = lastSyncDate
			)
		ID =  RetailProductGroups.objects.get(code=code)
		return ID.id

def create_or_update_attributes(**kwargs):
	code = kwargs['code']
	description = kwargs['description']
	valueType = kwargs['valueType']
	date_created = kwargs['date_created']
	lastSyncDate = kwargs['lastSyncDate']

	get_attributes = Attributes.objects.filter(code=code)
	if not get_attributes:
		create_attrib = Attributes.objects.create(
				code=code,
				description=description,
				valueType=valueType,
				# date_created=date_created,
				lastSyncDate = lastSyncDate
			)
		ID =  Attributes.objects.get(code=code)
		return ID.id

	else:
		update_attrib = Attributes.objects.filter(code=code).update(
				code=code,
				description=description,
				valueType=valueType,
				lastSyncDate = lastSyncDate
			)
		ID =  Attributes.objects.get(code=code)
		return ID.id

def create_or_update_attrib_opt_values(**kwargs):
	option_value = kwargs['option_value']
	date_created = kwargs['date_created']
	lastSyncDate = kwargs['lastSyncDate']

	get_attrib_opt_value = AttributeOptionValues.objects.filter(option_value=option_value)
	if not get_attrib_opt_value:
		create_att_opt = AttributeOptionValues.objects.create(
				option_value=option_value,
				# date_created=date_created,
				lastSyncDate = lastSyncDate
			)
		ID = AttributeOptionValues.objects.get(option_value=option_value)
		return ID.id

	else:
		update_att_opt = AttributeOptionValues.objects.filter(option_value=option_value).update(
				option_value=option_value,
				lastSyncDate = lastSyncDate
			)
		ID = AttributeOptionValues.objects.get(option_value=option_value)
		return ID.id



def create_or_update_sales_price(**kwargs):
	salesCode = kwargs['salesCode']
	unitPrice2 = kwargs['unitPrice2']
	unitPriceIncludingVAT = kwargs['unitPriceIncludingVAT']

	minimumQuantity = kwargs['minimumQuantity']
	endingDate = kwargs['endingDate']
	startingDate = kwargs['startingDate']
	date_created = kwargs['date_created']
	lastSyncDate = kwargs['lastSyncDate']

	get_sales_price = ItemPrices.objects.filter(salesCode=salesCode)
	if not get_sales_price:
		create_sales_price = ItemPrices.objects.create(
				salesCode=salesCode,
				unitPrice2=unitPrice2,
				unitPriceIncludingVAT=unitPriceIncludingVAT,
				minimumQuantity=minimumQuantity,
				startingDate=startingDate,
				endingDate=endingDate,
				# date_created=date_created,
				lastSyncDate = lastSyncDate
			)
		ID =  ItemPrices.objects.get(salesCode=salesCode)
		return ID.id

	else:
		update_sales_price = ItemPrices.objects.filter(salesCode=salesCode).update(
				salesCode=salesCode,
				unitPrice2=unitPrice2,
				unitPriceIncludingVAT=unitPriceIncludingVAT,
				minimumQuantity=minimumQuantity,
				startingDate=startingDate,
				endingDate=endingDate,
				lastSyncDate = lastSyncDate
			)
		ID =  ItemPrices.objects.get(salesCode=salesCode)
		return ID.id

def create_or_update_attrib_values(**kwargs):
	attributeCode = kwargs['attributeCode']
	attributeValue = kwargs['attributeValue']
	numericValue = kwargs['numericValue']
	valueCalculated = kwargs['valueCalculated']
	hardAttribute = kwargs['hardAttribute']
	date_created = kwargs['date_created']
	lastSyncDate = kwargs['lastSyncDate']

	get_attrib_value = AttributeValues.objects.filter(attributeCode_id=attributeCode,attributeValue_id=attributeValue)
	if not get_attrib_value:
		create_att_value = AttributeValues.objects.create(
				attributeCode_id=attributeCode,
				attributeValue_id=attributeValue,
				numericValue=numericValue,
				valueCalculated=valueCalculated,
				hardAttribute=hardAttribute,
				lastSyncDate = lastSyncDate
			)
		ID =  AttributeValues.objects.get(attributeCode_id=attributeCode,attributeValue=attributeValue)
		return ID.id
	else:
		update_att_value = AttributeValues.objects.filter(attributeCode_id=attributeCode,attributeValue_id=attributeValue).update(
				attributeCode_id=attributeCode,
				attributeValue_id=attributeValue,
				numericValue=numericValue,
				valueCalculated=valueCalculated,
				hardAttribute=hardAttribute,
				lastSyncDate = lastSyncDate
			)
		ID =  AttributeValues.objects.get(attributeCode_id=attributeCode,attributeValue=attributeValue)
		return ID.id

def create_or_update_item_translation(**kwargs):
	variantCode = kwargs['variantCode']
	languageCode = kwargs['languageCode']
	description = kwargs['description']
	description2 = kwargs['description2']
	date_created = kwargs['date_created']
	lastSyncDate = kwargs['lastSyncDate']

	get_item_translation = ItemTranslations.objects.filter(variantCode=variantCode)
	if not get_item_translation:
		create_item_translation = ItemTranslations.objects.create(
				variantCode=variantCode,
				languageCode=languageCode,
				description=description,
				description2=description2,
				# date_created=date_created,
				lastSyncDate = lastSyncDate
			)
		ID =  ItemTranslations.objects.get(variantCode=variantCode)
		return ID.id

	else:
		update_item_translation = ItemTranslations.objects.filter(variantCode=variantCode).update(

				languageCode=languageCode,
				description=description,
				description2=description2,
				lastSyncDate = lastSyncDate
			)
		ID =  ItemTranslations.objects.get(variantCode=variantCode)
		return ID.id


def create_or_update_nav_item(**kwargs):
	no = kwargs['no']
	description = kwargs['description']
	retailProductGroup_id = kwargs['retailProductGroup']
	baseUnitOfMeasure = kwargs['baseUnitOfMeasure']
	salesUnitOfMeasure = kwargs['salesUnitOfMeasure']
	itemCategoryCode = kwargs['itemCategoryCode']
	unitPrice = kwargs['unitPrice']
	itemPrice = kwargs['itemPrice']
	grossWeight = kwargs['grossWeight']
	netWeight = kwargs['netWeight']
	blocked = kwargs['blocked']
	inventory = kwargs['inventory']
	width = kwargs['width']
	height = kwargs['height']
	depth = kwargs['depth']
	attributeValue = kwargs['attributeValue']
	lastSyncDate = kwargs['lastSyncDate']

	get_nav_item = NavItem.objects.filter(no=no)
	if not get_nav_item:
		create_nav_item = NavItem.objects.create(
				no=no,
				description=description,
				# retailProductGroup_id=retailProductGroup_id,
				baseUnitOfMeasure=baseUnitOfMeasure,
				salesUnitOfMeasure=salesUnitOfMeasure,
				itemCategoryCode=itemCategoryCode,
				unitPrice=unitPrice,
				grossWeight=grossWeight,
				netWeight=netWeight,
				blocked=blocked,
				inventory=inventory,
				width=width,
				height=height,
				depth=depth,
				lastSyncDate = lastSyncDate
			)
		# ID_ATTRIB =  AttributeValues.objects.get(attributeCode_id=attributeCode,attributeValue=attributeValue)
		if len(attributeValue) > 0:
			create_nav_item.attributeValue.add(*attributeValue)
		if retailProductGroup_id is not None:
			create_nav_item.retailProductGroup_id=retailProductGroup_id

		create_nav_item.itemPrice.add(*itemPrice)

		return create_nav_item

	else:
		update_nav_item = NavItem.objects.get(no=no)
		update_nav_item.description=description
		update_nav_item.baseUnitOfMeasure=baseUnitOfMeasure
		update_nav_item.salesUnitOfMeasure=salesUnitOfMeasure
		update_nav_item.itemCategoryCode=itemCategoryCode
		update_nav_item.unitPrice=unitPrice
		update_nav_item.grossWeight=grossWeight
		update_nav_item.netWeight=netWeight
		update_nav_item.blocked=blocked
		update_nav_item.inventory=inventory
		update_nav_item.width=width
		update_nav_item.height=height
		update_nav_item.depth=depth
		update_nav_item.lastSyncDate = lastSyncDate
		update_nav_item.attributeValue.clear()
		if len(attributeValue) > 0:
			update_nav_item.attributeValue.clear()

			update_nav_item.attributeValue.add(*attributeValue)
		if retailProductGroup_id is not None:
			update_nav_item.retailProductGroup_id = retailProductGroup_id

		update_nav_item.itemPrice.clear()
		update_nav_item.itemPrice.add(*itemPrice)
		update_nav_item.save()
		return update_nav_item