from nav_wordpress.models import NavItem
from nav_wordpress.navitem import (create_or_update_local_item, create_or_update_product_group,
                                   item_hierarchy, createUpdateAllProduct)
from webapp.celery import app as celery_app


@celery_app.task

def nav_crud_task(items):
    print("_________________TEST________")
    for i in range(len(items)):
        create_update = create_or_update_local_item(items[i])
        print(create_update)


def create_local_prod_task(items):
    print("_________________TEST________")
    for i in range(len(items)):
        create_update = create_or_update_local_item(no=items[i])
        print(create_update)


@celery_app.task
def task2():
    print("________Task2____")


@celery_app.task
def create_prod_group_task():
    create_or_update_product_group()


@celery_app.task
def createUpdateAllProductTask():
    createUpdateAllProduct()
