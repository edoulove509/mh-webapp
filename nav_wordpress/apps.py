from django.apps import AppConfig


class NavWordpressConfig(AppConfig):
    name = 'nav_wordpress'
