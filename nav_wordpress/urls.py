from django.urls import path, re_path, include
from . import views

urlpatterns = [
    path('local-product/', views.local_product, name='local-product'),
    path('upload/', views.upload_image, name='upload-image'),
    path('upload/success', views.upload_success, name='upload-success'),
    path('task/', views.task, name='task'),
    path('galery/', views.galery, name='galery'),

]
