import django
import hashlib, binascii, os
from datetime import datetime
import datetime
import glob
import requests
from webapp.config import TOP_URL_IMG, CONSUMER_KEY, CONSUMER_SECRET, TOP_URL_PRODUCT, WCAPI


def NOW():
    return django.utils.timezone.now()


def local_time(date_time):
    return django.utils.timezone.localtime(date_time)


# for format number to real sku MH
def format_sku(item):
    final_list = []

    for num in item:
        pos = str(num).find('.')
        # underscore = str(num).find("_")
        # if underscore == -1:
        if num[0] == '1' and len(num[:pos]) == 7:
            final_list.append(num[:pos])
    return final_list

def namefromjpg(name):
        pos = str(name).find('.jpg')
        final_name = name[:pos]
        if pos != -1:
            return final_name
        else:
            return name


def hash_password(password):
    """Hash a password for storing."""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                  salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')


def passwordHached(stored_password, provided_password):
    """Verify a stored password against one provided by user"""
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512',
                                  provided_password.encode('utf-8'),
                                  salt.encode('ascii'),
                                  100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password


def existFolder(path):
    date = datetime.now()
    year = date.year
    month = date.month
    day = date.day
    folder_name = os.path.basename(path)
    folder_date = str(day) + "-" + str(month) + "-" + str(year)
    if os.path.isdir(path):
        if str(folder_name) == folder_date:
            return True
        else:
            return False


def getJpgFile(path):
    jpgFileList = []
    allJpg = glob.glob(path + "/*jpg", recursive=False)
    print(allJpg)

    for file in allJpg:
        image = os.path.basename(file)
        jpgFileList.append(image)

    return jpgFileList


def itemExistOnWp(item):
    req = WCAPI.get('products', params={'sku': item}).json()
    if len(req) == 1:
        return True
    elif len(req) == 0:
        return False


def getNumListdata(path):
    list_num = []
    lines = []
    with open(path) as f:
        lines = f.readlines()
    if len(lines[0]) > 0:
        if '...' in lines[0]:
            line1 = str(lines[0])
            num_inf = int(line1[:7])
            num_sup = int(line1[len(line1) - 7:len(line1)])
            for i in range(num_inf, num_sup + 1):
                list_num.append(i)
        return list_num
    elif len(lines) >= 2:
        for line in lines:
            list_num.append(line)
        return list_num


def c_bytes(number_of_bytes, unit='bytes'):
    if number_of_bytes < 0:
        return number_of_bytes
    step_to_greater_unit = 1024.

    number_of_bytes = float(number_of_bytes)
    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'Kb'

    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'Mb'

    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'Gb'

    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'Tb'

    number_of_bytes = round(number_of_bytes, 1)

    return str(number_of_bytes) + ' ' + unit


def get_id_image(name):
    link = TOP_URL_IMG + "media/?slug=" + name
    img_req = requests.get(link)
    if img_req.status_code == 200:
        img_json_response = img_req.json()
        id_img = img_json_response[0]['id']
        return id_img


# CHECK IMAGE SET FOR PRODUCT
def check_image_set(item):
    req = WCAPI.get('products', params={'sku': item}).json()
    print(req)
    if len(req) > 0:
        if len(req[0]['images']) > 0:
            for im in req[0]['images']:
                if item == im['name']:
                    return True
                else:
                    return False


# CHECK CATEGORY SET FOR PRODUCT
def check_category_set(item):
    req = WCAPI.get('products', params={'sku': item}).json()
    print(req)

    if len(req) > 0:
        if len(req[0]['categories']) > 0:

            return [True, req[0]['id']]
        else:
            return [False, req[0]['id']]
    else:
        print("There is no data")
