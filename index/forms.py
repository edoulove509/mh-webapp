from django import forms
from django.utils.translation import ugettext_lazy as _


class LoginForm(forms.Form):
    email = forms.EmailField(max_length=255,
                             widget=forms.EmailInput(
                                 attrs={'class': 'form-control', "placeholder": _("Email Adress")}))

    password = forms.CharField(max_length=130,
                               widget=forms.PasswordInput(
                                   attrs={'class': 'form-control ', 'placeholder': _('Password')}))

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')

        if not password:
            self.add_error('password', _('The password field is required'))

        if not email:
            self.add_error('email', _('Please provide your Email'))
