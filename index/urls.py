from django.urls import path, re_path, include
from . import views

urlpatterns = [
    path('', views.home, name='index'),
    path('login', views.signin, name='signin'),
    path('logout', views.signout, name='logout'),
    # path('index/', views.index, name='index'),
    re_path(r'^celery-progress/', include('celery_progress.urls')),
]
