from django.shortcuts import render, redirect, HttpResponseRedirect
from .forms import LoginForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.conf import settings


def home(request):
    user = request.user
    if not user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    else:
        return render(request, 'index/index.html')


def signin(request):
    if request.user.is_authenticated:
        return redirect('/')
    context = {}
    url = request.GET.get("next")
    form = LoginForm(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            email = request.POST.get("email", "").lower().strip()
            password = request.POST.get("password")

            user = authenticate(request, email=email, password=password)
            if user is not None:
                login(request, user)
                return redirect("index")
            else:
                form = LoginForm(request.POST or None)

        else:
            print("Form is invalid")
            messages.add_message(request, messages.WARNING, "Unknown user: Email or password is incorrect!")

    context['form'] = form
    return render(request, "index/signin.html", context)


def signout(request):
    logout(request)
    if request.method == "POST":
        print(request.session.items())
        logout(request)
        return redirect('signin')
    return HttpResponseRedirect("/login/")

def index(request):
    return render(request,"index/index.html")