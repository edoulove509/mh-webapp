from django.conf import settings
from django.utils.deprecation import MiddlewareMixin


def process_exception(request, exception):
	if settings.DEBUG:
		print(exception.__class__.__name__)
		print(exception.message)
	return None


class StartupMiddleware(MiddlewareMixin):
	pass
