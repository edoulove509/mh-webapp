from django.shortcuts import render
from .NavItemHierarchy import *


# Create your views here.

def nav_product(request):
    item_hierarchy = NavItemHierarchy()
    nav_item = item_hierarchy.get_items(field='', criteria='', size='100')
    context = {'nav_item': nav_item}
    # print(nav_item['AttributeValues']['Attribute_Values'])
    return render(request, "nav/nav_product.html", context)
