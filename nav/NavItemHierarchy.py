from zeep import Client
from zeep.transports import Transport
from requests import Session
from requests.auth import HTTPBasicAuth


class NavItemHierarchy(object):
    def __init__(self):
        # Nav connection parameters
        self.retail_item_wsdl = 'http://mh1test:7047/NAV/WS/Maison%20Handal/Page/WSRetailItemCard?WSDL'
        self.product_group_wsdl = 'http://mh1test:7047/NAV/WS/Maison%20Handal/Page/WSRetailProductGroups?WSDL'
        self.attributes_wsdl = 'http://mh1test:7047/NAV/WS/Maison%20Handal/Page/WSAttributeCard?WSDL'

        # self.retail_item_wsdl = 'http://www.learnwebservices.com/services/hello?WSDL'
        # self.product_group_wsdl = 'http://www.learnwebservices.com/services/hello?WSDL'
        # self.attributes_wsdl = 'http://www.learnwebservices.com/services/hello?WSDL'

        self.session = Session()
        self.session.auth = HTTPBasicAuth('pjjeanbaptiste', '11Password!')
        self.session.verify = False
        self.retail_item_api = Client(wsdl=self.retail_item_wsdl, transport=Transport(session=self.session))
        self.product_group_api = Client(wsdl=self.product_group_wsdl, transport=Transport(session=self.session))
        self.attributes_api = Client(wsdl=self.attributes_wsdl, transport=Transport(session=self.session))

    def get_items(self, **kwargs):
        # Request filter object
        if 'no' in kwargs:
            no = kwargs['no']
            try:
                response = self.retail_item_api.service.Read(no)
                if len(response) > 0:
                    return response
            except Exception as e:
                print("ERROR")

        elif 'field' and 'criteria' in kwargs:
            field = kwargs['field']
            criteria = kwargs['criteria']
            size = kwargs['size']
            request_data = {
                'filter': {
                    'Field': field,
                    'Criteria': criteria,
                },
                'setSize': size
            }
            response = self.retail_item_api.service.ReadMultiple(**request_data)
            if len(response) > 0:
                return response

    def get_product_groups(self, **kwargs):
        product_group_code = kwargs['product_group_code']
        code = kwargs['code']
        # Request filter object
        request_data = {
            'filter': {
                'Field': code,
                'Criteria': product_group_code
            },
            'setSize': '0'
        }

        response = self.product_group_api.service.ReadMultiple(**request_data)
        return response
